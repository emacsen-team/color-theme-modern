Source: color-theme-modern
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 12)
             , dh-elpa
Standards-Version: 4.6.1
Homepage: https://github.com/emacs-jp/replace-colorthemes
Vcs-Browser: https://salsa.debian.org/emacsen-team/color-theme-modern
Vcs-Git: https://salsa.debian.org/emacsen-team/color-theme-modern.git

Package: elpa-color-theme-modern
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs
Enhances: emacs
Description: deftheme reimplementation of classic Emacs color-themes
 Color-theme-modern is a port of many of the classic Emacs color-themes.
 Many of these themes were previously part of emacs-goodies-el.  Screenshots
 are provided in the documentation directly.  The full list is as follows:
 .
 aalto-dark, aalto-light, aliceblue, andreas, arjen, beige-diff,
 beige-eshell, bharadwaj-slate, bharadwaj, billw, black-on-gray,
 blippblopp, blue-erc, blue-eshell, blue-gnus, blue-mood, blue-sea,
 calm-forest, charcoal-black, clarity, classic, cobalt, comidia
 dark-blue2, dark-blue, dark-erc, dark-font-lock, dark-gnus,
 dark-green, dark-info, dark-laptop, deep-blue, desert, digital-ofs1,
 emacs-21, emacs-nw, euphoria, feng-shui, fischmeister
 .
 gnome2, gnome, goldenrod, gray1, gray30, greiner, gtk-ide,
 high-contrast, hober, infodoc, jb-simple, jedit-grey,
 jonadabian-slate, jonadabian, jsc-dark, jsc-light2, jsc-light, julie,
 katester, kingsajz, late-night, lawrence, ld-dark, lethe, marine,
 marquardt, matrix, midnight, mistyday, montz, oswald, parus, pierson,
 pok-wob, pok-wog, railscast, ramangalahy, raspopovic, renegade,
 resolve, retro-green, retro-orange, robin-hood, rotor, ryerson,
 .
 salmon-diff, salmon-font-lock, scintilla, shaman, simple-1,
 sitaramv-nt, sitaramv-solaris, snowish, snow, standard-ediff,
 standard, subdued, subtle-blue, subtle-hacker, taming-mr-arneson,
 taylor, tty-dark, vim-colors, whateveryouwant, wheat,
 word-perfect, xemacs, xp
